/* CMinus language lexer specification */

%%

%public
%class CMinusScanner
%implements IScanner

%unicode
%type Token
%standalone

%line
%column

%{
  StringBuilder string = new StringBuilder();

  Token nextToken;
  
  private Token token(TokenType type) {
    return new Token(type, yyline+1, yycolumn+1);
  }

  private Token token(TokenType type, Object value) {
    return new Token(type, yyline+1, yycolumn+1, value);
  }

  /** 
   * assumes correct representation of a long value for 
   * specified radix in scanner buffer from <code>start</code> 
   * to <code>end</code> 
   */
  private long parseLong(int start, int end, int radix) {
    long result = 0;
    long digit;

    for (int i = start; i < end; i++) {
      digit  = Character.digit(yycharat(i),radix);
      result*= radix;
      result+= digit;
    }

    return result;
  }
%}

/* main character classes */
LineTerminator = \r|\n|\r\n

WhiteSpace = {LineTerminator} | [ \t\f]

/* identifiers */
Identifier = [:jletter:][:jletter:]*
BadIdentifier = [:jletter:][:jletter:]*[0-9][0-9]*

/* integer literals */
DecIntegerLiteral = 0 | [1-9][0-9]*
BadDecIntegerLiteral = (0 | [1-9][0-9]*)[:jletter:][:jletter:]*

/* Comment state */
%state COMMENT

%%

<YYINITIAL> {

  /* keywords */
  "else"                         { return token(ELSE_TOKEN); }
  "if"                           { return token(IF_TOKEN); }
  "int"                          { return token(INT_TOKEN); }
  "return"                       { return token(RETURN_TOKEN); }
  "void"                         { return token(VOID_TOKEN); }
  "while"                        { return token(WHILE_TOKEN); } 
  
  /* separators */
  "("                            { return token(OPEN_PARA_TOKEN); }
  ")"                            { return token(CLOSE_PARA_TOKEN); }
  "{"                            { return token(OPEN_CURLY_TOKEN); }
  "}"                            { return token(CLOSE_CURLY_TOKEN); }
  "["                            { return token(OPEN_BRACKET_TOKEN); }
  "]"                            { return token(CLOSE_BRACKET_TOKEN); }
  ";"                            { return token(SEMICOLON_TOKEN); }
  ","                            { return token(COMMA_TOKEN); }
  
  /* operators */
  "="                            { return token(ASSIGN_TOKEN); }
  ">"                            { return token(GREATER_TOKEN); }
  "<"                            { return token(LESS_TOKEN); }
  "=="                           { return token(EQUIVALENT_TOKEN); }
  "<="                           { return token(LESS_EQ_TOKEN); }
  ">="                           { return token(GREATER_EQ_TOKEN); }
  "!="                           { return token(NOT_EQUIVALENT_TOKEN); }
  "+"                            { return token(PLUS_TOKEN); }
  "-"                            { return token(MINUS_TOKEN); }
  "*"                            { return token(MULTIPLY_TOKEN); }
  "/"                            { return token(DIVIDE_TOKEN); }

  /* comment */
  "/*"                           { yybegin(COMMENT); }

  /* unexpected end of comment */
  "*/"                           { throw new RuntimeException("Unexpected end of comment at "+yyline+", column "+yycolumn); }

  /* numeric literals */

  /* This is matched together with the minus, because the number is too big to 
     be represented by a positive integer. */  
  {DecIntegerLiteral}            { return token(NUM_TOKEN, new Integer(yytext())); }
  {BadDecIntegerLiteral}     { throw new RuntimeException("Illegal Integer literal “+yytext()+” at line “+yyline”, column ”+yycolumn); }

  /* whitespace */
  {WhiteSpace}                   { /* ignore */ }

  /* identifiers */ 
  {Identifier}                   { return token(ID_TOKEN, yytext()); }
  {BadIdentifier}            { throw new RuntimeException("Illegal identifier “+yytext()+” at line “+yyline”, column ”+yycolumn); }
}

<COMMENT> {
  "*/"                           { yybegin(YYINITIAL); }
  EOF                            { throw new RuntimeException("Comment was never closed. End of file found"); }
  [^]                            { /* ignore */ }
}

[^]                              { throw new RuntimeException("Illegal character \""+yytext()+
                                                              "\" at line "+yyline+", column "+yycolumn); }

<<EOF>>                          { return token(EOF_TOKEN); }
