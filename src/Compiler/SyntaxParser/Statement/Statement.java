package Compiler.SyntaxParser.Statement;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Statement.java
  **/
public class Statement {
    private Statement statement;
    
    public Statement() {
        this(null);
    }
    
    public Statement(Statement statement) {
        this.statement = statement;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Statement {");
        
        this.statement.print(childIndent);
        
        printHelper(indent, "}");
    }
}
