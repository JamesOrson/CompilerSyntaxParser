package Compiler.SyntaxParser.Statement;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Expression.Expression;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: WhileStatement.java
  **/
public class WhileStatement extends Statement {
    private Expression whileExpression;
    private Statement whileStatement;
    
    public WhileStatement() {
        this(null, null);
    }
    
    public WhileStatement(Expression whileExpression, Statement whileStatement) {
        this.whileExpression = whileExpression;
        this.whileStatement = whileStatement;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "While Statement {");
        
        printHelper(childIndent, "while");
        printHelper(childIndent, "(");
        this.whileExpression.print(childIndent);
        printHelper(childIndent, ")");
        this.whileStatement.print(childIndent);
        
        printHelper(indent, "}");
    }
}
