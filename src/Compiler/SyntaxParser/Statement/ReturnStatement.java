package Compiler.SyntaxParser.Statement;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Expression.Expression;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: ReturnStatement.java
  **/
public class ReturnStatement extends Statement {
    private Expression returnExpression;
    
    public ReturnStatement() {
        this(null);
    }
    
    public ReturnStatement(Expression returnExpression) {
        this.returnExpression = returnExpression;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Return Statement {");
        
        printHelper(childIndent, "return");
        if (this.returnExpression != null) {
            this.returnExpression.print(childIndent);
        }
        printHelper(childIndent, ";");
        
        printHelper(indent, "}");
    }
}
