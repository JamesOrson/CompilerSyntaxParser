package Compiler.SyntaxParser.Decl;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: VarDecl.java
  **/
public class VarDecl extends Decl {
    private boolean hasBrackets;
    private Integer index;
    
    public VarDecl() {
        this(null, false, -1);
    }
    
    public VarDecl(String id) {
        this(id, false, -1);
    }
    
    public VarDecl(String id, boolean hasBrackets, int index) {
        this.typeSpecifier = "int";
        this.id = id;
        this.hasBrackets = hasBrackets;
        this.index = index;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Var Decl {");

        printHelper(childIndent, this.typeSpecifier);
        printHelper(childIndent, this.id);
        if (this.hasBrackets) {
            printHelper(childIndent, "[");
            printHelper(childIndent, this.index.toString());
            printHelper(childIndent, "]");
        }
        printHelper(childIndent, ";");

        printHelper(indent, "}");
    }
}
