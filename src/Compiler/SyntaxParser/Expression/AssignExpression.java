package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: AssignExpression.java
  **/
public class AssignExpression extends Expression {
    private VarExpression leftHandSide;
    private Expression rightHandSide;
    
    public AssignExpression(VarExpression leftHandSide, Expression rightHandSide) {
        this.leftHandSide = leftHandSide;
        this.rightHandSide = rightHandSide;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Assign Expression {");
        
        this.leftHandSide.print(childIndent);
        printHelper(childIndent, "=");
        this.rightHandSide.print(childIndent);
        
        printHelper(indent, "}");
    }
}
