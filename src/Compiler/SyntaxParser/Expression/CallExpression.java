package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import java.util.ArrayList;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: CallExpression.java
  **/
public class CallExpression extends Expression {
    private String id;
    private ArrayList<Expression> args;
    
    public CallExpression(String id, ArrayList<Expression> args) {
        this.id = id;
        this.args = args;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Call Expression {");
        
        printHelper(childIndent, id);
        printHelper(childIndent, "(");
        if (!this.args.isEmpty()) {
            args.get(0).print(childIndent);
            for (int i = 1; i < this.args.size(); ++i) {
                printHelper(childIndent, ",");
                args.get(i).print(childIndent);
            }
        }
        printHelper(childIndent, ")");
        
        printHelper(indent, "}");
    }
}
