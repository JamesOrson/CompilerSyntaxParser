package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: VarExpression.java
  **/
public class VarExpression extends Expression {
    private String id;
    private boolean hasBrackets;
    private Expression bracketExpression;
    
    public VarExpression(String id) {
        this(id, false, null);
    }
    
    public VarExpression(String id, boolean hasBrackets, Expression bracketExpression) {
        this.id = id;
        this.hasBrackets = hasBrackets;
        this.bracketExpression = bracketExpression;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Var Expression {");
        
        printHelper(childIndent, id);
        if (this.hasBrackets) {
            printHelper(childIndent, "[");
            this.bracketExpression.print(childIndent);
            printHelper(childIndent, "]");
        }
        
        printHelper(indent, "}");
    }
}
