package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Expression.java
  **/
public class Expression {
    protected boolean hasParens;
    private Expression expression;
    
    public Expression() {
        this(false, null);
    }
    
    public Expression(boolean hasParens, Expression expression) {
        this.hasParens = hasParens;
        this.expression = expression;
    }
    
    public void setHasParens(boolean hasParens) {
        this.hasParens = hasParens;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Expression {");
        
        if (this.expression != null) {
            if (this.hasParens) {
                printHelper(childIndent, "(");
            }
            this.expression.print(childIndent);
            if (this.hasParens) {
                printHelper(childIndent, ")");
            }
        }
        
        printHelper(indent, "}");
    }
}
